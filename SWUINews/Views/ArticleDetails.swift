//
//  ArticleDetails.swift
//  SWUINews
//
//  Created by Ferenc Knebl on 2022. 04. 20..
//

import SwiftUI

struct ArticleDetails: View {
    @ObservedObject private var imageModel = UrlImageModel()
    var article: Article

    init(article: Article) {
        self.article = article
        if let imageUrl = article.urlToImage {
            imageModel.downloadImage(from: imageUrl)
        }
    }

    var body: some View {
        NavigationView {
            ScrollView(.vertical) {
                VStack(alignment: .leading) {
                    Text(article.title)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.title)
                        .padding(.leading)
                        .padding(.trailing)
                    Image(uiImage: imageModel.image ?? UIImage())
                        .resizable()
                        .scaledToFill()
                        .frame(height: 200)
                        .clipped()
                        .overlay(Divider(), alignment: .top)
                        .shadow(color: .black.opacity(0.2), radius: 5, y: 6)
                    Text(article.content)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.body)
                        .padding(.leading)
                        .padding(.trailing)
                    Spacer()
                }
            }
            .navigationBarTitle(Text(""))
            .navigationBarHidden(true)
        }.navigationViewStyle(.stack)
    }
}

struct ArticleDetails_Previews: PreviewProvider {
    static var previews: some View {
        ArticleDetails(article: Article(source: .init(id: "123", name: "engadget"), author: "author", title: "title", articleDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", url: "url", urlToImage: nil, publishedAt: Date(), content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a ante dui. Nunc a dolor vel tortor interdum convallis eu non turpis. Donec tincidunt, ante et rutrum rutrum, risus ante tincidunt nibh, in convallis metus elit a odio. Integer dignissim urna sed risus tempus iaculis. Etiam quis odio a elit pharetra auctor. Sed velit nulla, iaculis cursus metus non, mattis iaculis ligula. Fusce non odio ex. Vestibulum aliquet egestas purus et mollis. Mauris tristique, tortor vitae lacinia molestie, sapien felis sagittis urna, in vestibulum magna enim sed arcu. Nulla aliquam rhoncus varius. Pellentesque sit amet orci a ligula pharetra sollicitudin sit amet at enim. Donec elementum fermentum nisi ac porta. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas non mattis magna. Quisque mauris massa, bibendum mattis tortor id, imperdiet lacinia purus. Integer elementum dolor quis tortor bibendum efficitur."))
    }
}

import SwiftUI

struct NewsArticleRow: View {
    var title: String
    @ObservedObject private var imageModel = UrlImageModel()

    init(title: String, imageUrl: String? = nil) {
        self.title = title
        if let imageUrl = imageUrl {
            imageModel.downloadImage(from: imageUrl)
        }
    }

    var body: some View {
        HStack {
            Image(uiImage: imageModel.image ?? UIImage())
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 30, height: 30)
                .clipped()
                .padding(.trailing, 5)
            Text(title)
                .font(.system(size: 12))
                .lineLimit(2)
            Spacer()
        }
        .padding(.leading, 5)
        .padding(.trailing, 5)
    }
}

struct NewsArticleRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NewsArticleRow(title: "Lorem\nLorem")
            NewsArticleRow(title: "Lorem\nLorem")
        }
        .previewLayout(.fixed(width: 300, height: 70))

    }
}

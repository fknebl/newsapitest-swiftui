import Foundation
import UIKit

extension MemoryImageCache {
    @available(iOS 13, *)
    func downloadImage(from url: String) async throws -> UIImage? {
        return try await withCheckedThrowingContinuation({ [weak self] continuation in
            self?.downloadImage(from: url, completion: { url, image, error  in
                if let image = image {
                    continuation.resume(returning: image)
                } else {
                    if let error = error {
                        continuation.resume(throwing: error)
                    } else {
                        continuation.resume(returning: nil)
                    }
                }
            })
        })
    }
}

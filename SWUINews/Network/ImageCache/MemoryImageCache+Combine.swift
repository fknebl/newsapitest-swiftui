import Combine
import Foundation
import UIKit

extension MemoryImageCache {
    @available(iOS 13, *)
    func downloadImage(from url: String) -> Future<UIImage?, Error> {
        Future() { [weak self] promise in
            self?.downloadImage(from: url) { url, image, error in
                if let error = error {
                    promise(Result.failure(error))
                } else {
                    promise(Result.success(image))
                }
            }
        }
    }
}

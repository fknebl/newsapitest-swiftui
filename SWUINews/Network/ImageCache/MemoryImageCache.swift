import Foundation
import UIKit

typealias CacheImageCompletion = (String, UIImage?, Error?) -> ()
typealias CacheRequest = (String) -> (URLRequest?)
typealias ImageCreator = (Data) -> (UIImage?)

class MemoryImageCache {
    let cache = NSCache<NSString, UIImage>()
    var requestUrlsLock = NSLock()
    var session = URLSession.shared
    var cacheRequest: CacheRequest = {
        guard let url = URL(string: $0) else {
            return nil
        }
        return URLRequest(url: url)
    }
    var imageCreator: ImageCreator = {
        let scale = UIScreen.main.scale
        return UIImage(data: $0, scale: scale)
    }

    init(countLimit: Int = 10) {
        cache.countLimit = countLimit
    }

    func downloadImage(from url: String, completion: @escaping CacheImageCompletion) {
        let cachedImage = cache.object(forKey: url as NSString)

        if let image = cachedImage {
            completion(url, image, nil)
        } else {
            // TODO: store completion closures for url to avoid multi downloads
            guard let cacheRequest = cacheRequest(url) else {
                return
            }
            session.dataTask(with: cacheRequest) { [weak self] data, response, error in
                guard let data = data, let image = self?.imageCreator(data) else {
                    completion(url, nil, error)
                    return
                }
                self?.cache.setObject(image, forKey: url as NSString)
                completion(url, image, nil)
            }.resume()
        }
    }

    func clear() {
        cache.removeAllObjects()
    }
}

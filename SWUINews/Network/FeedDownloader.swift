import Foundation

class FeedDownloader {
    func downloadFeed(query: String) async throws -> Feed {
        let escapedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        let url = URL(string: "\(Constants.baseUrl)/everything?apiKey=\(Constants.serviceApiKey)&q=\(escapedQuery)")!
        let (data, _) = try await URLSession.shared.data(from: url)
        let decoder = JSONDecoder()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        decoder.dateDecodingStrategy = .formatted(dateFormat)
        return try decoder.decode(Feed.self, from: data)
    }
}

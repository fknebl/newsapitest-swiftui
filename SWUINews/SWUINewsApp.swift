//
//  SWUINewsApp.swift
//  SWUINews
//
//  Created by Ferenc Knebl on 2022. 04. 19..
//

import SwiftUI

@main
struct SWUINewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

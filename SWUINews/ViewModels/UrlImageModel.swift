import Combine
import UIKit

class UrlImageModel: ObservableObject {
    @Published var image: UIImage?
    var cancellable = Set<AnyCancellable>()

    func downloadImage(from url: String) {
        ImageCache.shared.downloadImage(from: url)
            .receive(on: DispatchQueue.main)
            .sink { _ in

            } receiveValue: { [weak self] image in
                self?.image = image
            }.store(in: &cancellable)
    }

    func cancelDownloads() {
        cancellable.removeAll()
    }
}

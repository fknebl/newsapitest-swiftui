//
//  ModelData.swift
//  SWUINews
//
//  Created by Ferenc Knebl on 2022. 04. 23..
//

import Foundation

class ContentViewModel: ObservableObject {
    var query: String = ""
    let feedDownloader = FeedDownloader()
    @Published private(set) var articles: [Article] = []

    @MainActor
    func loadArticles() async throws {
        try await loadArticles(query: query)
    }

    @MainActor
    func loadArticles(query: String) async throws {
        self.query = query
        articles = try await feedDownloader.downloadFeed(query: query).articles
    }
}

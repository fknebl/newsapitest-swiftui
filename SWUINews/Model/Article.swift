//
//  Article.swift
//  SWUINews
//
//  Created by Ferenc Knebl on 2022. 04. 23..
//

import Foundation

struct Article: Codable, Identifiable {
    var id: Date {
        publishedAt
    }
    var source: ArticleSource
    var author: String?
    var title: String
    var articleDescription: String
    var url: String
    var urlToImage: String?
    var publishedAt: Date
    var content: String

    enum CodingKeys: String, CodingKey {
        case source, author, title
        case articleDescription = "description"
        case url, urlToImage, publishedAt, content
    }

    struct ArticleSource: Codable {
        var id: String?
        var name: String
    }
}


import Foundation

struct Feed: Codable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}

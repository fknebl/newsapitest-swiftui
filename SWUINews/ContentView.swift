//
//  ContentView.swift
//  SWUINews
//
//  Created by Ferenc Knebl on 2022. 04. 19..
//

import SwiftUI

// TODO: Localization
struct ContentView: View {
    @State private var downloadError = false
    @StateObject private var model = ContentViewModel()

    var body: some View {
        NavigationView {
            List(model.articles) { article in
                NavigationLink(destination: ArticleDetails(article: article)) {
                    NewsArticleRow(
                        title: article.title,
                        imageUrl: article.urlToImage)
                }
            }
            .task {
                if model.articles.isEmpty {
                    await refreshArticles()
                }
            }
            .refreshable {
                await refreshArticles()
            }
            .listStyle(.plain)
            .alert("Failed to download", isPresented: $downloadError) {
                Button("OK", role: .cancel) { }
            }
            .navigationBarTitle(Text("News Feed"), displayMode: .inline)
        }.navigationViewStyle(.stack)
    }

    func refreshArticles() async {
        do {
            try await model.loadArticles(query: "tesla")
        } catch let error {
            print(error)
            downloadError = true
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPod touch (7th generation)", "iPhone 13 Pro Max"], id: \.self) { deviceName in
            ContentView()
                        .previewDevice(PreviewDevice(rawValue: deviceName))
                        .previewDisplayName(deviceName)
        }
    }
}
